#ifndef __CCZ_UTILS_H__
#define __CCZ_UTILS_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" 
{
#endif

/* 
    Compress in-memory data into CCZ format, allocates buffer *dst with the size
    equal to the size of uncompressed data. Returns actual size of data within
    the output buffer.
*/
unsigned long ccz_compress(const uint8_t* src, uint32_t size, uint8_t** dst);

#ifdef __cplusplus
}
#endif

#endif /* __CCZ_UTILS_H__ */