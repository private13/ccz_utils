#include "ccz_utils.h"

#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#define IS_BIG_ENDIAN (*((const uint16_t*)"\0\x1") == (uint16_t)1U)
#define SWAP_32(n) ((n&0xffU)<<24 | (n&0xff00U)<<8 | (n&0xff0000U)>>8 | (n&0xff000000U)>>24)
#define SWAP_16(n) ((n&0xffU)<<8  | (n&0xff00U)>>8)
#define BE32_2_HOST(n) ((IS_BIG_ENDIAN) ? n : SWAP_32(n))
#define BE16_2_HOST(n) ((IS_BIG_ENDIAN) ? n : SWAP_16(n))

struct ccz_head
{
    uint8_t magic[4];
    uint16_t type;
    uint16_t version;
    uint32_t reserved;
    uint32_t size;
};

static const uint8_t CCZ_MAGIC[] = {'C', 'C', 'Z', '!'};

unsigned long ccz_compress(const uint8_t* src, uint32_t size, uint8_t** dst)
{
    int zlib_res = 0;
    struct ccz_head* header;
    unsigned long dst_size = size;

    /* Allocate memory for the output plus header*/
    *dst = (uint8_t*) malloc(dst_size + sizeof(struct ccz_head));
    if (!(*dst))
        return 0;

    /* Compress data to allocated buffer, reserve space for header */
    zlib_res = compress((Bytef*) (*dst + sizeof(struct ccz_head)), 
                        &dst_size, (const Bytef*) src, size);
    if (zlib_res != Z_OK)
    {
        free(*dst);
        *dst = 0;
        return 0;
    }

    /* Fill first 16 bytes with header data */
    header = (struct ccz_head*) *dst;
    header->size = BE32_2_HOST(size);
    header->version = BE16_2_HOST(2U);
    memcpy(header, CCZ_MAGIC, sizeof(CCZ_MAGIC));
    header->reserved = 0U;
    header->type = 0U;

    dst_size += sizeof(struct ccz_head);
    return dst_size;
}
