#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "ccz_utils.h"

int main(int argc, char* argv[])
{
    int retval = 0;
    uint8_t* buffer = 0;
    uint8_t* result = 0;
    uint32_t file_size = 0;
    unsigned long output = 0;
    FILE* fp = fopen(argv[1], "r");
    FILE* op = fopen(argv[2], "w");

    fseek(fp, 0, SEEK_END);
    file_size = (ftell(fp) > 0) ? (uint32_t)ftell(fp) : 0U;
    if (!file_size)
    {
        printf("Memory allocation error\n");
        goto cleanup;
    }
    fseek(fp, 0, SEEK_SET);
    
    buffer = (uint8_t*) malloc(file_size);
    if (fread(buffer, sizeof(uint8_t), file_size, fp) != file_size)
    {
        printf("Error while reading input file\n");
        goto cleanup;
    }

    output = ccz_compress(buffer, file_size, &result);
    if (fwrite(result, sizeof(uint8_t), output, op) != output)
    {
        printf("Error while writing output file\n");
    }

cleanup:
    free(result);
    free(buffer);
    fclose(fp);
    fclose(op);
    return retval;
}